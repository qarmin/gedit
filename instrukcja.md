### Compilation in Docker

```
docker run -it ubuntu:rolling /bin/bash

sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

apt update
apt install -y git
git clone https://gitlab.gnome.org/GNOME/gedit.git
cd gedit

#Z#
apt build-dep -y gedit
apt install -y meson ninja-build git

git clone https://gitlab.gnome.org/GNOME/tepl.git telp
cd telp
apt build-dep -y libtepl-4-dev
meson build
ninja -C build/
ninja -C build/ install
cd ..
rm -rf telp

meson build 

ninja -C build

```

### QTCreator Includes
```

```
